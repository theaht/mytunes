# myTunes

This project consists of two parts. The first part is a web application.  
[Link to Thea and Camilla's app](https://whispering-escarpment-75776.herokuapp.com/) 

### Web application

The web application contains two views. The index page, and a page for displaying search results.

#### Index page

The index view are displaying 5 random tracks, artists and genres. These random picks will update every time the page is reloaded.
If the music note symbol in the upper left corner is clicked, the page will reload. 

There is also a search field where the user can type in a string, to search for tracks where the track name contains or is equal to the string. The search is not case-sensitive. 

The default value of the search field is set to 'never', meaning if the user click 'search' without typing anything, the search result will be all track names containing the word 'never'. 

#### Search results page 

The other view is displaying all songs matching the search criteria from the search field at the index page. 

Together with the track name, the artist, album and genre of the song is displayed. 

If the user click on the arrow in the upper left corner, they will be navigated to the index page. 

### REST endpoints 

The second part exposes REST endpoints consisting of JSON data. The data is collected from the SQLite database called chinook. There are six endpoints with the following functionality: 

ENDPOINT | Method | Functionality 
--- | --- | ---
api/customers | GET | Presenting all the customers 
api/customers/add | POST | Add a new customer 
api/customer/update/{customerId} | PUT | Update an exsisting customer 
api/customers/count/by/country | GET | Presenting number of customers per country 
api/customers/biggest/spenders | GET | Presenting ocustomers ordered descending by whos is the biggest spenders 
api/customer/{customerId}/popular/genre | GET | Presents the most popular genre/genres of a given customer

### Postman collection
The JSON postman collection file can be found in the folder 'Postman', in the root directory of the project. There is one file with response examples and one file without response examples. 


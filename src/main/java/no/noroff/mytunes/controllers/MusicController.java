package no.noroff.mytunes.controllers;

import no.noroff.mytunes.dataaccess.MusicRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MusicController {

    final private MusicRepository musicRepository = new MusicRepository();

    /**
     * This returns the index-view.
     * Lists of tracks, artists and genre added to the model, so that the information will be
     * available in the view.
     * */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model){
        model.addAttribute("artists", musicRepository.get5RandomArtists());
        model.addAttribute("tracks", musicRepository.get5RandomTracks());
        model.addAttribute("genres", musicRepository.get5RandomGenres());
        return "index";
    }

    /**
     * Returns the searchresult-view.
     * It uses the requested parameter to search for all tracks containing this term.
     * The list of all these tracks are added to the model, so that all this information will be
     * available in the view.
     * */
    @RequestMapping(value="/search", method = RequestMethod.GET)
    public String searchResults(Model model, @RequestParam(defaultValue="never") String term){
        model.addAttribute("results", musicRepository.getTracksFromSearch(term));
        return "searchresults";
    }


}

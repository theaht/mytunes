package no.noroff.mytunes.controllers;

import no.noroff.mytunes.dataaccess.CustomerRepository;
import no.noroff.mytunes.models.Country;
import no.noroff.mytunes.models.Customer;
import no.noroff.mytunes.models.Genre;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {

    final private CustomerRepository customerRepository = new CustomerRepository();

    /**
     * This returns a list of all the customers in the database
     * */
    @RequestMapping(value="/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers(){
        return customerRepository.selectAllCustomers();
    }

    /**
     * This adds a new customer.
     * The new customer data is in the body of the request.
     * */
    @RequestMapping(value = "api/customers/add", method = RequestMethod.POST)
    public Boolean addNewCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }

    /**
     * This updates an existing customer
     * It takes the new customer data from the body of the request.
     * */
    @RequestMapping(value = "api/customer/update/{customerId}", method = RequestMethod.PUT)
    public Boolean updateCustomer(@PathVariable int customerId, @RequestBody Customer customer){
        return customerRepository.updateCustomer(customer);
    }

    /**
     * This returns a list of the number of customers in each country.
     * The list is in descending order.
     * */
    @RequestMapping(value = "api/customers/count/by/country", method = RequestMethod.GET)
    public ArrayList<Country> getNumberOfCustomersPerCountry (){
        return customerRepository.getNumOfCustomersPerCountry();

    }

    /**
     * This returns a list of customers, sorted from the highest spender to the least spending customer.
     * */
    @RequestMapping(value = "api/customers/biggest/spenders", method = RequestMethod.GET)
    public ArrayList<Customer> getHighestSpenders(){
        return customerRepository.getHighestSpenders();
    }

    /**
     * This returns the most popular genre of a given customer.
     * If the customer has several equally popular genres, they are all returned.
     * */
    @RequestMapping(value = "api/customer/{customerId}/popular/genre", method = RequestMethod.GET)
    public ArrayList<Genre> getCustomerGenre(@PathVariable int customerId){
        return customerRepository.getCustomerGenre(customerId);

    }


}

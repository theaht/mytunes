package no.noroff.mytunes.dataaccess;

import no.noroff.mytunes.models.Artist;
import no.noroff.mytunes.models.Genre;
import no.noroff.mytunes.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class MusicRepository {

    //The url to the database
    private final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";

    private Connection connection = null;


    /**
     * This method connects to the database.
     * It then creates a query to get five random artists.
     * The query is executed and the result is added to a list of artists.
     * The connection is closed and the list of artists are returned.
     * */
    public ArrayList<Artist> get5RandomArtists(){

        ArrayList<Artist> artists = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement = connection.prepareStatement("SELECT Name FROM Artist ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                artists.add(new Artist( resultSet.getString("Name")));
            }

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
        return artists;
    }

    /**
     * This method connects to the database, then it creates a query to get
     * 5 random genres from the database.
     * The query is executed and the result is added to an arraylist of genres.
     * The connection is closed and the list of genres are returned.
     * */
    public ArrayList<Genre> get5RandomGenres(){

        ArrayList<Genre> genres = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement = connection.prepareStatement("SELECT Name FROM Genre ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                genres.add(new Genre( resultSet.getString("Name")));
            }

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
        return genres;
    }

    /**
     * This method connects to the database and
     * creates a query to get 5 random tracks.
     * The query is executed and result is added to a list of tracks
     * The connection is closed and list of tracks is returned.
     * */
    public ArrayList<Track> get5RandomTracks(){

        ArrayList<Track> tracks = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement = connection.prepareStatement("SELECT Name FROM Track ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                tracks.add(new Track( resultSet.getString("Name")));
            }

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
        return tracks;
    }


    /**
     * This method connects to the database.
     * A query is created and executed to get all tracks containing a given term, together with the
     * artist name, genre and album of the track. The search is case insensitive.
     * The result is added to a list of tracks.
     * The connection is closed and the list of track objects are returned.
     * @param search the given term to search for.
     * */
    public ArrayList<Track> getTracksFromSearch(String search){
        ArrayList<Track> tracks = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement = connection.prepareStatement("SELECT Track.Name, Artist.Name AS ArtistName, Album.Title, Genre.Name AS GenreName " +
                    "FROM Album " +
                    "INNER JOIN Track  ON Album.AlbumId = Track.AlbumId "+
                    "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                    "INNER JOIN Artist ON Album.ArtistId = Artist.ArtistId " +
                    "WHERE Track.Name LIKE '%'||?||'%' ");

            preparedStatement.setString(1, search);
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                tracks.add(new Track( resultSet.getString("Name"),
                        new Artist(resultSet.getString("ArtistName")),
                        resultSet.getString("Title"),
                        new Genre(resultSet.getString("GenreName"))
                        ));
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
        }

        return tracks;
    }

}

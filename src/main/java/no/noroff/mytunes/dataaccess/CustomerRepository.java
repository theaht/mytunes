package no.noroff.mytunes.dataaccess;

import no.noroff.mytunes.models.Country;
import no.noroff.mytunes.models.Customer;
import no.noroff.mytunes.models.Genre;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public class CustomerRepository{

    private final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite"; //URL to Chinook sqlite database
    private Connection connection = null;


    /**
     * This method connects to the database and creates a query for
     * finding all customers. The query is executed and the result
     * is made into customers who gets added to an arraylist of
     * customers that is returned.
     * */
    public ArrayList<Customer> selectAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<Customer>();
        try{
            connection = DriverManager.getConnection(URL); //Open Connection to database

            // Prepare Statement
            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer");
            ResultSet resultSet = preparedStatement.executeQuery(); // Execute Statement

            // Process Results
            while (resultSet.next()){ //while there are resultSets
                customers.add( //add to customersList
                        new Customer( //// the newly created customer with parameters customerId, FirstName, LastName, Cuntry, PostalCode, Phone, Email from resultSet
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        )
                );
            }

        }
        catch (Exception ex){ //catch exceptions
            //print exception
            System.out.println("Something went wrong!");
            System.out.println(ex.toString());
        }
        finally {
            try{
                connection.close(); // Close Connection
            }
            catch (Exception ex){ //catch exceptions when closing
                //print exception
                System.out.println("Something went wrong when closing connection.");
                System.out.println(ex.toString());
            }
            return customers; //return list of customers
        }
    }


    /**
     * This method takes in a customer, connects to the database and
     * creates a query to add the customer to the database. The query is
     * executed and the success of the execution is returned.
     * */
    public boolean addCustomer(Customer customer){
        Boolean success = false;
        try {
            connection = DriverManager.getConnection(URL); //Open Connection to database

            // Prepare Statement
            PreparedStatement preparedStatement =
                    connection.prepareStatement("INSERT INTO customer(CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (?, ?, ?, ?, ?, ?, ?)");
            //adding values to the queryString
            preparedStatement.setInt(1, customer.getCustomerID());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhoneNumber());
            preparedStatement.setString(7, customer.getEmail());

            int result = preparedStatement.executeUpdate(); // Execute Statement
            success = (result != 0); //check if the execution was successful

        }
        catch (Exception ex){ //catch exceptions
            //print exceptions
            System.out.println("Something went wrong!");
            System.out.println(ex.toString());
        }
        finally{
            try{
                connection.close(); //close connection
            }catch (Exception ex){//catch exceptions when closing
                //print exception
                System.out.println("Something went wrong when closing connection.");
                System.out.println(ex.toString());
            }
            return success; //return if the execution was successful
        }
    }


    /**
     * This method takes in a customer, connects to the database and creates
     * a query to update an existing user. The query is executed and the result
     * of the execution is returned.
     * */
    public Boolean updateCustomer(Customer customer){
        Boolean success = false;
        try {
            connection = DriverManager.getConnection(URL); //Open Connection to database

            // Prepare Statement
            PreparedStatement preparedStatement =
                    connection.prepareStatement("UPDATE Customer SET CustomerId = ?, FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId = ?");
            //adding values to the queryString
            preparedStatement.setInt(1, customer.getCustomerID());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhoneNumber());
            preparedStatement.setString(7, customer.getEmail());
            preparedStatement.setInt(8, customer.getCustomerID());

            int result = preparedStatement.executeUpdate();// Execute Statement
            success = (result != 0);//check if the execution was successful

        }
        catch (Exception e){//catch exceptions
            //print exception
            System.out.println(e.getMessage());
        }
        finally {
            try {
                connection.close(); //close connection
            }
            catch (Exception e){ //catch exceptions when closing
                //print exception
                System.out.println(e.getMessage());
            }
            return success; //return if the execution was successful
        }
    }


    /**
     * This method connects to the database and creates a query for
     * finding the number of customers in each country. The query is
     * executed and the result is made into countries who gets added
     * to an arraylist of countries which is returned.
     * */
    public ArrayList<Country> getNumOfCustomersPerCountry(){
        ArrayList<Country> countries = new ArrayList<>();
        try{
            connection = DriverManager.getConnection(URL);//Open Connection to database

            // Prepare Statement
            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT Country, COUNT(*) as NumberofCustomers FROM Customer GROUP BY Country ORDER BY NumberofCustomers DESC");
            ResultSet resultSet = preparedStatement.executeQuery(); // Execute Statement

            while(resultSet.next()){ //while there are resultSets
                countries.add( //add to countries-list
                        new Country( //// the newly created country with parameters Country and NumberOfCustomers from the resultSet
                                resultSet.getString("Country"),
                                resultSet.getInt("NumberOfCustomers")
                        )
                );
            }
        }
        catch (Exception e){ //catch exceptions
            //print exceptions
            System.out.println(e.getMessage());

        }
        finally {
            try {
                connection.close(); //close connection
            }
            catch (Exception e){ //catch exceptions when closing
                //print exceptions
                System.out.println(e.getMessage());
            }
            return countries; //return the list of countries
        }
    }


    /**
     * This method connects to the database and creates a query for
     * finding the customers with the highest total on invoice. The
     * query is executed and the result is made into customers who
     * gets added to an arraylist of customers which is returned.
     * */
    public ArrayList<Customer> getHighestSpenders(){
        ArrayList<Customer> customers = new ArrayList<Customer>();
        try{
            connection = DriverManager.getConnection(URL);//Open Connection to database

            // Prepare Statement
            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, Customer.Country, Customer.PostalCode, Customer.Phone, Customer.Email " +
                            "FROM Customer " +
                            "INNER JOIN Invoice ON Customer.CustomerId=Invoice.CustomerId " +
                            "ORDER BY Invoice.Total DESC;");
            ResultSet resultSet = preparedStatement.executeQuery(); // Execute Statement

            while (resultSet.next()){ //while there are resultSets
                customers.add( //add to customers-list
                        new Customer( // the newly created customer with parameters customerId, FirstName, LastName, Cuntry, PostalCode, Phone, Email from resultSet
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        )
                );
            }
        }
        catch (Exception ex){ //catch exceptions
            //print exceptions
            System.out.println("Something went wrong");
            System.out.println(ex.toString());
        }
        finally {
            try{
                connection.close(); //close connection
            }
            catch (Exception ex){ //catch exceptions when closing
                //print exceptions
                System.out.println("Something went wrong when closing connection.");
                System.out.println(ex.toString());
            }
            return customers; //return list of customers
        }
    }


    /**
     * This method connects to the database and creates a query for
     * finding all of the genres the customer has bought plu the
     * count of each genre. The query is executed and the result is
     * made into genres who gets added to an arraylist of genres.
     * The arraylist is traversed to find the top genre/genres who gets
     * returned in a new arraylist.
     * */
    public ArrayList<Genre> getCustomerGenre(int customerId){
        ArrayList<Genre> genres = new ArrayList<Genre>();
        ArrayList<Genre> topGenres = new ArrayList<Genre>();

        try{
            connection = DriverManager.getConnection(URL); //Open Connection to database

            // Prepare Statement
            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT Genre.Name, COUNT(*) AS GenreCount " +
                            "FROM Customer " +
                            "INNER JOIN Invoice ON Customer.CustomerId=Invoice.CustomerId " +
                            "INNER JOIN InvoiceLine ON Invoice.InvoiceId=InvoiceLine.InvoiceId " +
                            "INNER JOIN Track ON InvoiceLine.TrackId=Track.TrackId "+
                            "INNER JOIN Genre ON Track.GenreId=Genre.GenreId " +
                            "WHERE Customer.CustomerId=? " +
                            "GROUP BY Genre.Name;");
            preparedStatement.setInt(1, customerId); //adding CustomerId-value to the queryString
            ResultSet resultSet = preparedStatement.executeQuery(); // Execute Statement

            while (resultSet.next()) { //while there are resultSets
                genres.add( //add to genre-list
                        new Genre( // the newly created genre with parameters Name and GenreCount
                                resultSet.getString("Name"),
                                resultSet.getInt("GenreCount")
                        )
                );
            }

            // Finding the highest count of genres for all genres
            int highestCount = 0;
            for (Genre genre : genres){
                int count = genre.getGenreCount();
                if (count > highestCount){
                    highestCount = count;
                }
            }

            //finding the genre(s) with the highest count and adding it/them to the return-list topGenres
            for (int i = 0; i < genres.size(); i++){
                Genre genre = genres.get(i);
                int count = genre.getGenreCount();
                if (count == highestCount){
                    topGenres.add(genre);
                }
            }
        }
        catch (Exception ex){//catch exceptions
            //print exceptions
            System.out.println("Something went wrong");
            System.out.println(ex.toString());
        }
        finally {
            try{
                connection.close(); // Close connection
            }
            catch (Exception ex){ // Catch exceptions when closing
                //print exceptions
                System.out.println("Something went wrong when closing connection.");
                System.out.println(ex.toString());
            }
            return topGenres; //return topGenres list

        }
    }





}

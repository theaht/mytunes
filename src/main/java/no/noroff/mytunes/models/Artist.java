package no.noroff.mytunes.models;

public class Artist {

    private String name;

    /**
     * Constructor. Sets the name of the artist.
     * */
    public Artist(String name) {
        this.name = name;
    }

    /**
     * Get method for name.
     * */
    public String getName() {
        return name;
    }

}

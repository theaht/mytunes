package no.noroff.mytunes.models;

public class Track {
    private String name;
    private String albumName;
    private Artist artist;
    private Genre genre;

    /**
     * Constructor.
     * When a track is created with only name.
     * */
    public Track(String name){
        this.name = name;
    }

    /**
     * Constructor.
     * When a track is created with name, artist, album and genre.
     * */
    public Track(String name, Artist artist, String albumName,Genre genre){
        this.name = name;
        this.albumName = albumName;
        this.artist = artist;
        this.genre = genre;
    }


    /**
     * Getters and setters for all variables
     * */

    public String getAlbumName() {
        return albumName;
    }

    public Artist getArtist() {
        return artist;
    }

    public Genre getGenre() {
        return genre;
    }

    public String getName() {
        return name;
    }

    /**
     * Get method for the name of the artist.
     * Calls the getName method in the artist object.
     * */
    public String getArtistName(){
        return artist.getName();
    }

    /**
     * Get method for the name of the genre.
     * Calls the getName method in the genre object.
     * */
    public String getGenreName(){
        return genre.getName();
    }



}

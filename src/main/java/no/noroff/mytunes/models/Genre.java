package no.noroff.mytunes.models;

public class Genre {
    private String name;
    private int genreCount;

    /**
     * Constructor.
     * When a Genre is created with only name.
     * */
    public Genre(String name){
        this.name = name;
    }

    /**
     * Constructor.
     * When a genre is created with both name and count
     * */
    public Genre(String name, int genreCount){
        this.name = name;
        this.genreCount = genreCount;
    }

    /**
     * Getters and setters for name and count.
     * */
    public String getName() {
        return name;
    }


    public int getGenreCount() {
        return genreCount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGenreCount(int genreId) {
        this.genreCount = genreId;
    }
}

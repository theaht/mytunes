package no.noroff.mytunes.models;

public class Country {

    private String name;
    private int numberOfCustomers;

    /**
     * Constructor.
     * Sets the name of the country and the number of customers in this country.
     * */
    public Country(String name, int numberOfCustomers){
        this.name = name;
        this.numberOfCustomers = numberOfCustomers;
    }

    /**
     * Getters and setters for all the variables.
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }

    public void setNumberOfCustomers(int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }
}

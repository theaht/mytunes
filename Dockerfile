FROM openjdk:15
VOLUME tmp
ADD target/mytunes-1.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]